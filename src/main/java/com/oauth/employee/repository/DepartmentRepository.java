package com.oauth.employee.repository;

import com.oauth.employee.model.Department;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Integer> {
    @Override
    <S extends Department> S save(S s);

    @Override
    List<Department> findAll();

    @Override
    Optional<Department> findById(Integer integer);

    @Override
    void deleteById(Integer integer);

    @Override
    boolean existsById(Integer integer);
}