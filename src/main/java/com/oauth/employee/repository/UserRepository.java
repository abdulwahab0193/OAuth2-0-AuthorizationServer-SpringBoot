package com.oauth.employee.repository;

import com.oauth.employee.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Users, Integer> {
    Optional<Users> findByUsername(String username);

    @Override
    List<Users> findAll();

    @Override
    <S extends Users> S save(S s);

    @Override
    void delete(Users users);

    @Transactional
    @Modifying
    @Query("delete from Users u where u.username = ?1")
    void deleteUsersByUsername(String username);

    @Transactional
    @Modifying
    @Query("delete from Users u where u.id = ?1")
    void deleteByEId(int id);
}