package com.oauth.employee.repository;

import com.oauth.employee.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    @Override
    List<Employee> findAll();
    @Override
    <S extends Employee> S save(S s);
    @Override
    Optional<Employee> findById(Integer integer);
    @Override
    void deleteById(Integer integer);
    @Override
    boolean existsById(Integer integer);

    @Transactional
    @Modifying
    @Query("update Employee set picture = ?1 where id = ?2")
    void updatePicture(byte[] picture, int id);
}
