package com.oauth.employee.repository;

import com.oauth.employee.model.Designation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DesignationRepository extends JpaRepository<Designation, Integer> {
    @Override
    <S extends Designation> S save(S s);

    @Override
    List<Designation> findAll();

    @Override
    Optional<Designation> findById(Integer integer);

    @Override
    void deleteById(Integer integer);

    @Override
    boolean existsById(Integer integer);
}
