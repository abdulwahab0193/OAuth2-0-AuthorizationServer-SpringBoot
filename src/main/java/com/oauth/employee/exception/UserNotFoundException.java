package com.oauth.employee.exception;

public class UserNotFoundException extends RuntimeException{
    private static final long serialVersionUID = 2L;
}
