package com.oauth.employee.model;

import javax.persistence.*;

@SuppressWarnings({"RedundantSuppression", "unused"})
@Entity
@Table(name = "user")
public class Users {

    @Id
    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "e_id")
    private int id;

    public Users(){}

    public Users(Users user){
        this.username = user.username;
        this.password = user.password;
        this.id = user.id;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }
}
