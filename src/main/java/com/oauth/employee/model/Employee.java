package com.oauth.employee.model;

import javax.persistence.*;
import java.sql.Date;

@SuppressWarnings({"unused", "RedundantSuppression"})
@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

    @Column(name = "phone")
    private long phone;

    @Column(name = "dob")
    private Date dob;

    @Column(name = "salary")
    private int salary;

    @Column(name = "picture")
    private byte[] picture;

    @Column(name = "dept_id")
    private int dept_id;

    @Column(name = "desig_id")
    private int desig_id;

    public Employee() {}

    public Employee(Employee e){
        this.id = e.id;
        this.name = e.name;
        this.address = e.address;
        this.city = e.city;
        this.country = e.country;
        this.phone = e.phone;
        this.dob = e.dob;
        this.salary = e.salary;
        this.picture = e.picture;
        this.dept_id = e.dept_id;
        this.desig_id = e.desig_id;
    }

    public Employee(String name, long phone, Date dob, int salary) {
        this.name = name;
        this.phone = phone;
        this.dob = dob;
        this.salary = salary;
    }

    public Employee(int id, String name, long phone, Date dob, int salary) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.dob = dob;
        this.salary = salary;
    }

    public Employee(String name, String address, String city, String country, long phone, Date dob, int salary, int dept_id, int desig_id) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.country = country;
        this.phone = phone;
        this.dob = dob;
        this.salary = salary;
        this.dept_id = dept_id;
        this.desig_id = desig_id;
    }

    public Employee(int id, String name, String address, String city, String country, long phone, Date dob, int salary, byte[] picture, int dept_id, int desig_id) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.city = city;
        this.country = country;
        this.phone = phone;
        this.dob = dob;
        this.salary = salary;
        this.picture = picture;
        this.dept_id = dept_id;
        this.desig_id = desig_id;
    }

    public void updateValues(Employee e){
        if(e.name != null) this.name = e.name;
        if(e.address != null) this.address = e.address;
        if(e.city != null) this.city = e.city;
        if(e.country != null) this.country = e.country;
        if(e.phone != 0) this.phone = e.phone;
        if(e.dob != null) this.dob = e.dob;
        if(e.salary != 0) this.salary = e.salary;
        if(e.picture != null) this.picture = e.picture;
        if(e.dept_id != 0) this.dept_id = e.dept_id;
        if(e.desig_id != 0) this.desig_id = e.desig_id;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }

    public long getPhone() {
        return phone;
    }
    public void setPhone(long phone) {
        this.phone = phone;
    }

    public Date getDob() {
        return dob;
    }
    public void setDob(Date dob) {
        this.dob = dob;
    }

    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }

    public byte[] getPicture() {
        return picture;
    }
    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public int getDept_id() {
        return dept_id;
    }
    public void setDept_id(int dept_id) {
        this.dept_id = dept_id;
    }

    public int getDesig_id() {
        return desig_id;
    }
    public void setDesig_id(int desig_id) {
        this.desig_id = desig_id;
    }
}
