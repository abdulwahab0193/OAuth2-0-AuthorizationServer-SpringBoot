package com.oauth.employee.controller;

import com.oauth.employee.model.Users;
import com.oauth.employee.service.CustomUserDetailsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/user")
public class UserController {
    private final CustomUserDetailsService customUserDetailsService;
    private final PasswordEncoder passwordEncoder;

    public UserController(PasswordEncoder passwordEncoder, CustomUserDetailsService customUserDetailsService) {
        this.passwordEncoder = passwordEncoder;
        this.customUserDetailsService = customUserDetailsService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Object> getUsers(){
        return new ResponseEntity<>(customUserDetailsService.getUsers(), HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<Object> addUser(@RequestBody Users user){
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        customUserDetailsService.addUser(user);
        return new ResponseEntity<>("User added successfully.", HttpStatus.OK);
    }

    @RequestMapping(value = "/{username}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteUser(@PathVariable("username") String username){
        customUserDetailsService.deleteUser(username);
        return new ResponseEntity<>("User deleted Successfully.", HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteUser(Users user){
        customUserDetailsService.deleteUser(user);
        return new ResponseEntity<>("User deleted Successfully.", HttpStatus.OK);
    }
}
