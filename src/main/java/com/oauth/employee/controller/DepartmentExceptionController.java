package com.oauth.employee.controller;

import com.oauth.employee.exception.DepartmentNotFoundException;
import com.oauth.employee.exception.EmployeeInDepartmentException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class DepartmentExceptionController {
    @ExceptionHandler(value = DepartmentNotFoundException.class)
    public ResponseEntity<Object> departmentNotFound(DepartmentNotFoundException exception){
        return new ResponseEntity<>("No Department found under this ID.", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = EmployeeInDepartmentException.class)
    public ResponseEntity<Object> employeeInDepartment(EmployeeInDepartmentException exception){
        return new ResponseEntity<>("Cannot delete department. There are employees in given Department.", HttpStatus.CONFLICT);
    }
}
