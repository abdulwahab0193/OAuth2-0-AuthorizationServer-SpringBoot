package com.oauth.employee.controller;

import com.oauth.employee.model.Designation;
import com.oauth.employee.service.DesignationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/designation")
public class DesignationController {
    private final DesignationService designationService;

    public DesignationController(DesignationService designationService){
        this.designationService = designationService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Object> getDesignations(){
        return new ResponseEntity<>(designationService.getDesignations(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getDesignations(@PathVariable("id") int id){
        return new ResponseEntity<>(designationService.getDesignation(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<Object> addDesignation(@RequestBody Designation designation){
        designationService.addDesignation(designation);
        return new ResponseEntity<>("Designation added Successfully", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateDesignation(@PathVariable("id") int id, @RequestBody Designation designation){
        designation.setId(id);
        designationService.updateDesignation(id, designation);
        return new ResponseEntity<>("Designation updated Successfully", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteDesignation(@PathVariable("id") int id){
        designationService.deleteDesignation(id);
        return new ResponseEntity<>("Designation deleted Successfully", HttpStatus.OK);
    }
}
