package com.oauth.employee.controller;

import com.oauth.employee.exception.DesignationNotFoundException;
import com.oauth.employee.exception.EmployeeAtDesignationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class DesignationExceptionController {
    @ExceptionHandler(value = DesignationNotFoundException.class)
    public ResponseEntity<Object> departmentNotFound(DesignationNotFoundException exception){
        return new ResponseEntity<>("No Designation found under this ID.", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = EmployeeAtDesignationException.class)
    public ResponseEntity<Object> employeeAtDesignation(EmployeeAtDesignationException exception){
        return new ResponseEntity<>("Cannot delete designation. There are employees under given Designation.", HttpStatus.CONFLICT);
    }
}
