package com.oauth.employee.controller;

import com.oauth.employee.exception.EmployeeNotFoundException;
import com.oauth.employee.model.Employee;
import com.oauth.employee.service.CustomUserDetailsService;
import com.oauth.employee.service.EmployeeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {
    private final EmployeeService employeeService;
    private final CustomUserDetailsService customUserDetailsService;

    public EmployeeController(EmployeeService employeeService, CustomUserDetailsService customUserDetailsService) {
        this.employeeService = employeeService;
        this.customUserDetailsService = customUserDetailsService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Object> getEmployees(){
        return new ResponseEntity<>(employeeService.getEmployees(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getEmployees(@PathVariable("id") int id){
        employeeExists(id);
        return new ResponseEntity<>(employeeService.getEmployee(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<Object> addEmployee(@RequestBody Employee employee){
        employeeService.addEmployee(employee);
        return new ResponseEntity<>("Employee added Successfully", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateEmployee(@PathVariable("id") int id, @RequestBody Employee employee){
        return new ResponseEntity<>( employeeService.updateEmployee(id, employee), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteEmployee(@PathVariable("id") int id){

        employeeExists(id);
        customUserDetailsService.deleteUser(id);
        employeeService.deleteEmployee(id);
        return new ResponseEntity<>("Employee deleted Successfully", HttpStatus.OK);
    }

    @RequestMapping(value = "/picture/{id}", method = RequestMethod.POST)
    public ResponseEntity<Object> addPicture(@PathVariable("id") int id, @RequestBody MultipartFile picture) throws IOException {
        employeeExists(id);
        employeeService.updatePicture(picture.getBytes(), id);

        return new ResponseEntity<>("Picture updated Successfully", HttpStatus.OK);
    }

    private void employeeExists(int id){
        if(!employeeService.isEmployeePresent(id))
            throw new EmployeeNotFoundException();
    }
}