package com.oauth.employee.controller;

import com.oauth.employee.model.Department;
import com.oauth.employee.service.DepartmentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/department")
public class DepartmentController {
    private final DepartmentService departmentService;

    public DepartmentController(DepartmentService departmentService){
        this.departmentService = departmentService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Object> getDepartments(){
        return new ResponseEntity<>(departmentService.getDepartments(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getDepartment(@PathVariable("id") int id){
        return new ResponseEntity<>(departmentService.getDepartment(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<Object> addDepartment(@RequestBody Department department){
        departmentService.addDepartment(department);
        return new ResponseEntity<>("Department added Successfully", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateDepartment(@PathVariable("id") int id, @RequestBody Department department){
        department.setId(id);
        departmentService.updateDepartment(id, department);
        return new ResponseEntity<>("Department updated Successfully", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deleteDepartment(@PathVariable("id") int id){
        departmentService.deleteDepartment(id);
        return new ResponseEntity<>("Department deleted Successfully", HttpStatus.OK);
    }
}