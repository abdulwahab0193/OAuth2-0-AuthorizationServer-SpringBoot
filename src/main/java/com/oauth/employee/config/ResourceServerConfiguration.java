package com.oauth.employee.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationManager;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId("resource-server-rest-api")
                .authenticationManager(authenticationManagerBean())
                .tokenExtractor(new CustomTokenExtractor());
    }

    public AuthenticationManager authenticationManagerBean() {
        return new OAuth2AuthenticationManager();
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .disable()
                .anonymous()
                .and()
                .authorizeRequests()
                .antMatchers("/user/**", "/employee/**", "/department/**", "/designation/**")
                .authenticated()
                .antMatchers("/public/***")
                .permitAll()
                .and()
                .cors();
    }
}