package com.oauth.employee.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {
    @Bean
    public Docket employeeApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.oauth.employee.controller"))
                .paths(regex("/employee.*"))
                .build()
                .groupName("Employee");
    }

    @Bean
    public Docket userApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.oauth.employee.controller"))
                .paths(regex("/user.*"))
                .build()
                .groupName("User");
    }
    @Bean

    public Docket departmentApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.oauth.employee.controller"))
                .paths(regex("/department.*"))
                .build()
                .groupName("Department");
    }

    @Bean
    public Docket DesignationApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.oauth.employee.controller"))
                .paths(regex("/designation.*"))
                .build()
                .groupName("Designation");
    }
}