package com.oauth.employee.service;

import com.oauth.employee.model.Users;

import java.util.Collection;

public interface CustomUserDetailsService {
    Collection<Users> getUsers();
    void addUser(Users user);
    void deleteUser(String username);
    void deleteUser(Users user);
    void deleteUser(int eid);
}