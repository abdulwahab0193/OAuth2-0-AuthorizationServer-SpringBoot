package com.oauth.employee.service;

import com.oauth.employee.exception.DepartmentNotFoundException;
import com.oauth.employee.exception.EmployeeInDepartmentException;
import com.oauth.employee.model.Department;
import com.oauth.employee.repository.DepartmentRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class DepartmentServiceImpl implements DepartmentService{
    DepartmentRepository departmentRepository;

    public DepartmentServiceImpl(DepartmentRepository departmentRepository){
        this.departmentRepository = departmentRepository;
    }

    @Override
    public void addDepartment(Department department) {
        departmentRepository.save(department);
    }

    @Override
    public void updateDepartment(int id, Department department) {
        if(!departmentRepository.existsById(id))
            throw new DepartmentNotFoundException();

        departmentRepository.save(department);
    }

    @Override
    public void deleteDepartment(int id) {
        if(!departmentRepository.existsById(id))
            throw new DepartmentNotFoundException();
        try{
        departmentRepository.deleteById(id);
        } catch (Exception e){
            throw new EmployeeInDepartmentException();
        }
    }

    @Override
    public Optional<Department> getDepartment(int id) {
        if(!departmentRepository.existsById(id))
            throw new DepartmentNotFoundException();

        return departmentRepository.findById(id);
    }

    @Override
    public Collection<Department> getDepartments() {
        return departmentRepository.findAll();
    }

    @Override
    public boolean isDepartmentPresent(int id) {
        return departmentRepository.existsById(id);
    }
}