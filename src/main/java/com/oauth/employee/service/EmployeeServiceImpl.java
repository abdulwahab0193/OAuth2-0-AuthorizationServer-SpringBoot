package com.oauth.employee.service;

import com.oauth.employee.exception.DepartmentNotFoundException;
import com.oauth.employee.exception.DesignationNotFoundException;
import com.oauth.employee.exception.EmployeeNotFoundException;
import com.oauth.employee.model.Employee;
import com.oauth.employee.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final DepartmentService departmentService;
    private final DesignationService designationService;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository,
                               DepartmentService departmentService,
                               DesignationService designationService) {
        this.employeeRepository = employeeRepository;
        this.departmentService = departmentService;
        this.designationService = designationService;
    }

    @Override
    public void addEmployee(Employee employee) {
        if(!departmentService.isDepartmentPresent(employee.getDept_id()))
            throw new DepartmentNotFoundException();

        if(!designationService.isDesignationPresent(employee.getDesig_id()))
            throw new DesignationNotFoundException();


        employeeRepository.save(employee);
    }

    @Override
    public String updateEmployee(int id, Employee employee) {
        Optional<Employee> emp = employeeRepository.findById(id);
        if(emp.map(Employee::new).isEmpty())
            throw new EmployeeNotFoundException();

        emp.get().updateValues(employee);
        employeeRepository.save(emp.get());

        return "Employee Updated successfully.";
    }

    @Override
    public void deleteEmployee(int id) {
        employeeRepository.deleteById(id);
    }

    @Override
    public Optional<Employee> getEmployee(int id) {
        return employeeRepository.findById(id);
    }

    @Override
    public Collection<Employee> getEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public boolean isEmployeePresent(int id) {
        return employeeRepository.existsById(id);
    }

    @Override
    public void updatePicture(byte[] picture, int id) {
        employeeRepository.updatePicture(picture, id);
    }
}