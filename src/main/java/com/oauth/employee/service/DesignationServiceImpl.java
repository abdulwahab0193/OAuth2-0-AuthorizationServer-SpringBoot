package com.oauth.employee.service;

import com.oauth.employee.exception.DesignationNotFoundException;
import com.oauth.employee.exception.EmployeeAtDesignationException;
import com.oauth.employee.model.Designation;
import com.oauth.employee.repository.DesignationRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class DesignationServiceImpl implements DesignationService{
    DesignationRepository designationRepository;

    public DesignationServiceImpl(DesignationRepository designationRepository){
        this.designationRepository = designationRepository;
    }

    @Override
    public void addDesignation(Designation designation) {
        designationRepository.save(designation);
    }

    @Override
    public void updateDesignation(int id, Designation designation) {
        if(!designationRepository.existsById(id))
            throw new DesignationNotFoundException();

        designationRepository.save(designation);
    }

    @Override
    public void deleteDesignation(int id) {
        if(!designationRepository.existsById(id))
            throw new DesignationNotFoundException();

        try{
            designationRepository.deleteById(id);
        } catch (Exception e){
            throw new EmployeeAtDesignationException();
        }
    }

    @Override
    public Optional<Designation> getDesignation(int id) {
        if(!designationRepository.existsById(id))
            throw new DesignationNotFoundException();

        return designationRepository.findById(id);
    }

    @Override
    public Collection<Designation> getDesignations() {
        return designationRepository.findAll();
    }

    @Override
    public boolean isDesignationPresent(int id) {
        return designationRepository.existsById(id);
    }
}
