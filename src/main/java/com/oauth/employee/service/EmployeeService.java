package com.oauth.employee.service;

import com.oauth.employee.model.Employee;
import java.util.Collection;
import java.util.Optional;

public interface EmployeeService {
    void addEmployee(Employee employee);
    String updateEmployee(int id, Employee employee);
    void deleteEmployee(int id);
    Optional<Employee> getEmployee(int id);
    Collection<Employee> getEmployees();
    boolean isEmployeePresent(int id);
    void updatePicture(byte[] picture, int id);
}