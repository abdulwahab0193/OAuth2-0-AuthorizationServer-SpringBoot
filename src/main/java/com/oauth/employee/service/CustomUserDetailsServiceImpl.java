package com.oauth.employee.service;

import com.oauth.employee.exception.EmployeeNotFoundException;
import com.oauth.employee.exception.UserNotFoundException;
import com.oauth.employee.model.CustomUserDetails;
import com.oauth.employee.model.Users;
import com.oauth.employee.repository.EmployeeRepository;
import com.oauth.employee.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class CustomUserDetailsServiceImpl implements CustomUserDetailsService, UserDetailsService {

    private final UserRepository userRepository;
    private final EmployeeRepository employeeRepository;

    public CustomUserDetailsServiceImpl(UserRepository userRepository, EmployeeRepository employeeRepository) {
        this.userRepository = userRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Users> usersOptional = userRepository.findByUsername(username);

        usersOptional
                .orElseThrow(() -> new UsernameNotFoundException("Username not Found!"));

        return usersOptional
                .map(CustomUserDetails::new)
                .get();
    }

    @Override
    public Collection<Users> getUsers(){
        return userRepository.findAll();
    }

    @Override
    public void addUser(Users user) {
        if(!employeeRepository.existsById(user.getId()))
            throw new EmployeeNotFoundException();

        userRepository.save(user);
    }

    @Override
    public void deleteUser(String username) {
        if(userRepository.findByUsername(username).isEmpty())
            throw new UserNotFoundException();

        userRepository.deleteUsersByUsername(username);
    }

    @Override
    public void deleteUser(Users user) {
        if(userRepository.findByUsername(user.getUsername()).isEmpty())
            throw new UserNotFoundException();

        userRepository.delete(user);
    }

    @Override
    public void deleteUser(int eid) {
        userRepository.deleteByEId(eid);
    }
}