package com.oauth.employee.service;

import com.oauth.employee.model.Department;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public interface DepartmentService {
    void addDepartment(Department department);
    void updateDepartment(int id, Department department);
    void deleteDepartment(int id);
    Optional<Department> getDepartment(int id);
    Collection<Department>getDepartments();
    boolean isDepartmentPresent(int id);
}