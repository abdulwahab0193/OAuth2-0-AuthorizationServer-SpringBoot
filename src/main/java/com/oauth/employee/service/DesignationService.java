package com.oauth.employee.service;

import com.oauth.employee.model.Designation;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public interface DesignationService {
    void addDesignation(Designation department);
    void updateDesignation(int id, Designation department);
    void deleteDesignation(int id);
    Optional<Designation> getDesignation(int id);
    Collection<Designation> getDesignations();
    boolean isDesignationPresent(int id);
}