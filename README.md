# POC Employee

POC Employee is a Java Spring boot based project, which allows user to manage Employees with ease. It allows all necessary operations required for managing employees, such as handling Employees, Departments, Designation, Roles etc.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
### Tools & Technologies

POC Employee uses multiple 
    
```
    Spring Boot
    Spring Security
    Spring Web
    Liquibase
    JPA (which implicitly use Hibernate)
    Swagger
    MYSQL Database
    IntelliJ
    JAVA 11
```

### Prerequisites

POC Employee will require following suite of software/libraries inorder to run properly e.g.

```
    JDK 11
    mySQL
    IDE (IntelliJ Preffered)    
```

### Installation

Follow these steps to get your development env running

1.  Create a new database via your db env

    ```
        CREATE DATABASE <db_name>
    ```
   
   **Note:** _Rest of the DB Schema will be generated automatically by liquibase library. DB Schema can be checked in liquibase changelog files found in project._
   
2.  Update database configuration in **application.properties** file

    ```
        spring.datasource.url = jdbc:mysql://localhost:3306/<db_name>
        spring.datasource.username = <sql_username>
        spring.datasource.password = <sql_password>
    ```

## Testing

POC Employee can be tested using postman, or any respective client. All operations/endpoint requires an access token for successful working.

### Get Access Token:
 
Follow these steps to get access token [for Postman]

1. use **Basic Auth** in Authorization and fill following info

    ```
        Username = <ClientID>
        Password = <ClientSecret>
    ```
   
   In our case,
   
   ```
    ClientID = oauthClient
    ClientSecret = oauthSecret
   ```
   
   These configurations can be changed in **AuthorizationServiceConfiguration.java** class.
   
2.  Provide grant_type, username, and password inside RequestBody [Body]

    ```
        grant_type = password
        username = <username_db>
        password = <password_db>
    ```
    
    Default user(admin) has following configurations
    
    ```
        username = UserA
        password = passA
    ```
    or you can add another user via database console.
  
3. Send **Post** request to 

    ```
        http://localhost:8081/oauth/token
    ```
   
4. The response will be in the form of

    ```
        {
            "access_token": "<token_value>",
            "token_type": "bearer",
            "expires_in": 43199,
            "scope": "read"
        }
    ```

### Controller Operations:

Now, that we have access token. Send **access token** with each request to get desired response.

#### Example:

Following is the tutorial to fetch all Employees information.

1.  Create a new request with Authorization as type **OAuth 2.0** and
   
    ```
        Access Token = <token_value>
    ```
    
2.  Send a **GET** request to 
   
       ```
           http://localhost:8081/employee/ 
       ```
      
    This will return information of all employees saved in Database.
    
## Authors

-   **[Abdul Wahab Asif](https://github.com/AbdulWahab019)** - *Development*
-   **[Zeb Mustafa](https://github.com/ZebMustafa)** - *Supervision*

## Acknowledgments

-   *Muhammad Abuzar*